# SteamTools

Hi! SteamTools is a bot, built in discord.js, for accessing data from the Steam API, or even just from the Steam site, and displaying it in an easy to understand message in discord. All of SteamTools commands are easy to understand and use, and documentation will be provided in this document, as well as on the bots description on any bot list out there.


# Adding SteamTools

SteamTools can be easily added to any Discord Server with this invite URL:
[discordapp.com](https://discordapp.com/oauth2/authorize?client_id=379109549514293248&scope=bot&permissions=117760)

## Configuration

SteamTools is a fairly simple bot and does not require any setup to run smoothly. If you wish, the command prefix of SteamTools can be changed by running `@SteamTools#5143 prefix` followed by whatever you want to set the prefix to. Its recommended that the prefix be something not usually used in normal conversations, as nobody wants a bot going off whenever a common phrase is typed!

## Reporting Problems

SteamTools is still a WIP bot. Most commands added to the non-testing version should be stable and work 99.9% of the time, but in the off chance that an error does occur, feel free to message me, just like the error message says. It would be amazing if a screenshot of the error message is provided with the bug report, as well as the command the error occurred on.
#### An Error Example
```
@bObO, An error occurred while running the command: DiscordAPIError: Invalid Form Body
embed.thumbnail.url: Not a well formed URL.
You shouldn't ever receive an error like this.
Please contact bObO#1055.
```

# Commands

SteamTools has a list of commands that is available by running `@SteamTools#5143 help` or `[preifx] help`, and for some more detailed docs head to [steamtools.pw](https://steamtools.pw/stats).

# Other Important Stuff

Again, if there are any questions, please feel free to add me as a friend on discord, with the tag `bObO#1055`. This should not change, but in case it does, I will update the doc to reflect the change.

## Uses of SteamTools
If you plan on using any code SteamTools, I would ask that you do not publish the code on any bot list or website. This is just simple courtesy, I have put countless hours into SteamTools, and will continue to do so, and as such I ask that if you want to use steam tools on your own server as your own bot, **do not make it a public bot**.

## Contributions

Anybody can help me out with this project, feel free to submit a pull request and I will review the request in full detail. Contributions are very, very welcome.

## License
This Project is under a GNU AGPLv3 License, so if you use my work, please give credit to me. You can read more under the LICENSE.md file. Basically what this means is that if you use _*any*_ of my work, you need to give me credit, so please do.
