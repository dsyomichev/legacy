const {
  Command
} = require('discord.js-commando');
const st = require('../../modules/steamtools')
module.exports = class steamCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'item',
      group: 'community',
      memberName: 'item',
      description: 'Searches for an item on the Steam Market',
      examples: ['item <item name>'],
      guildOnly: true,
      throttling: {
        usages: 2,
        duration: 3,
      },
      args: [{
        key: "item",
        prompt: "What item would you like to search for?",
        type: "string"
      }]
    });
  }
  async run(msg, {
    item
  }) {
    let cc = msg.client.provider.get(msg.guild, "location")
    let search = await msg.embed(st.search())
    let result = await st.exItem(item, cc)
    if (result === -1) {
      return search.edit({
        embed: st.itemError()
      })
    }

    return search.edit({
      embed: {
        author: {
          name: result.name,
          icon_url: 'https://steamtools.pw/images/files/embed/logo.png'
        },
        thumbnail: {
          url: result.image
        },
        title: 'Steam Market',
        fields: [{
          name: "Lowest Listing :",
          value: result.sale,
          inline: true
        }, {
          name: "Highest Order :",
          value: result.order,
          inline: true
        }],
        color: 0x161182,
        footer: {
          text: "Game : " + result.game
        }
      }
    })








  }
}