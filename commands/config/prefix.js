const {
  Command
} = require('discord.js-commando');

module.exports = class PrefixCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'prefix',
      group: 'config',
      memberName: 'prefix',
      description: 'Shows or sets the command prefix.',
      format: '[prefix [new prefix]/"default"/"none"]',
      details: `If no prefix is provided, the current prefix will be shown.\nIf the prefix is "default", the prefix will be reset to the bot's default prefix.\nIf the prefix is "none", the prefix will be removed entirely, only allowing mentions to run commands.\nOnly administrators may change the prefix.
			`,
      examples: ['prefix', 'prefix -', 'prefix omg!', 'prefix default', 'prefix none'],

      args: [{
        key: 'prefix',
        prompt: 'What would you like to set the bot\'s prefix to?',
        type: 'string',
        max: 15,
        default: ''
      }]
    });
  }

  async run(msg, args) {
    // Just output the prefix
    if (!args.prefix) {
      const prefix = msg.guild ? msg.guild.commandPrefix : this.client.commandPrefix;
      return msg.embed({
        color: 0x170420,
        author: {
          name: "SteamTools",
          icon_url: 'https://steamtools.pw/images/files/embed/logo.png',
        },
        description: `${prefix ? `SteamTools Prefix :  \`${prefix}\`` : 'No Prefix'} \nYou can use ${msg.anyUsage('command')} to run commands.`
      })
    }

    // Check the user's permission before changing anything
    if (msg.guild) {
      if (!msg.member.hasPermission('ADMINISTRATOR') && !this.client.isOwner(msg.author)) {
        return msg.embed({
          color: 0x170420,
          author: {
            name: "SteamTools",
            icon_url: 'https://steamtools.pw/images/files/embed/logo.png',
          },
          description: 'Only administrators may change the command prefix!'
        })
      }
    }

    // Save the prefix
    const lowercase = args.prefix.toLowerCase();
    const prefix = lowercase === 'none' ? '' : args.prefix;
    let response;
    if (lowercase === 'default') {
      if (msg.guild) msg.guild.commandPrefix = null;
      else this.client.commandPrefix = null;
      const current = this.client.commandPrefix ? `\`${this.client.commandPrefix}\`` : 'no prefix';
      response = `Reset the command prefix to the default (currently ${current}).`;
    } else {
      if (msg.guild) msg.guild.commandPrefix = prefix;
      else this.client.commandPrefix = prefix;
      response = prefix ? `Prefix set to : \`${args.prefix}\`` : 'Removed the prefix.';
    }
    msg.embed({
      color: 0x170420,
      author: {
        name: "SteamTools",
        icon_url: 'https://steamtools.pw/images/files/embed/logo.png',
      },
      description: `${response} \nYou can use ${msg.anyUsage('command')} to run commands.`
    })
    return null;
  }
};