const {
  stripIndents,
  oneLine
} = require('common-tags');
const {
  Command
} = require('discord.js-commando');

function disambiguation(items, label, property = 'name') {
  const itemList = items.map(item => `_*${(property ? item[property] : item).replace(/ /g, '\xa0')}*_`).join(' | ');
  return `${itemList}`;
}


module.exports = class HelpCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'help',
      group: 'info',
      memberName: 'help',
      aliases: ['commands'],
      description: 'Displays a list of available commands, or detailed information for a specified command.',
      details: oneLine `
				The command may be part of a command name or a whole command name.
				If it isn't specified, all available commands will be listed.
			`,
      examples: ['help', 'help prefix'],
      guarded: true,

      args: [{
        key: 'command',
        prompt: 'Which command would you like to view the help for?',
        type: 'string',
        default: ''
      }]
    });
  }

  async run(msg, args) { // eslint-disable-line complexity
    const groups = this.client.registry.groups;
    const commands = this.client.registry.findCommands(args.command, false, msg);
    const showAll = args.command && args.command.toLowerCase() === 'all';
    if (args.command && !showAll) {
      if (commands.length === 1) {
        let fields = []
        fields.push({
          name: 'Format : ',
          value: msg.anyUsage(`${commands[0].name}${commands[0].format ? ` ${commands[0].format}` : ''}`)
        })
        if (commands[0].aliases.length > 0) fields.push({
          name: 'Alisases : ',
          value: `**Aliases:** ${commands[0].aliases.join(', ')}`
        });
        fields.push({
          name: 'Groups : ',
          value: `${commands[0].group.name}(\`${commands[0].groupID}:${commands[0].memberName}\`)`
        })
        if (commands[0].details) fields.push({
          name: 'Details : ',
          value: commands[0].details
        });
        if (commands[0].examples) fields.push({
          name: 'Examples',
          value: `\`${commands[0].examples.join('\n')}\``
        })
        return msg.embed({
          color: 0x170420,
          author: {
            name: 'SteamTools',
            icon_url: 'https://steamtools.pw/images/files/embed/logo.png',
          },
          description: `**${commands[0].name.charAt(0).toUpperCase() + commands[0].name.slice(1)}** : ${commands[0].description}`,
          fields: fields
        })
      } else if (commands.length > 1) {
        return msg.embed({
          color: 0x170420,
          author: {
            name: "SteamTools",
            icon_url: 'https://steamtools.pw/images/files/embed/logo.png',
          },
          description: ` ** Multiple Commands **: ${disambiguation(commands, 'commands')}\nPlease be more specific.`
        })
      } else {
        return msg.embed({
          color: 0x170420,
          author: {
            name: "SteamTools",
            icon_url: 'https://steamtools.pw/images/files/embed/logo.png',
          },
          description: ` ** Unknown Command **: \nUse $ {
          msg.usage(
            null, msg.channel.type === 'dm' ? null : undefined, msg.channel.type === 'dm' ? null : undefined
          )
        }
        to view the list of available commands.
        `
        })
      }
    } else {
      const messages = [];

      return msg.embed({
        color: 0x170420,
        author: {
          name: "SteamTools",
          icon_url: 'https://steamtools.pw/images/files/embed/logo.png',
        },
        description: ` ** Documentation **: https://steamtools.pw/docs\n\n**Support Server** : https://discord.gg/T4ptNEu\n\n**Help Command** : \`help [command name]\``
      })
    }
  }
};
