const {
  Command
} = require('discord.js-commando');

module.exports = class inviteCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'invite',
      group: 'info',
      memberName: 'invite',
      description: 'Sends an invite link for the bot.',
      examples: ['invite'],
      guildOnly: true,
      throttling: {
        usages: 2,
        duration: 3
      }
    });
  }

  run(msg) {
    return msg.embed({
      color: 0x170420,
      author: {
        name: "SteamTools",
        icon_url: 'https://steamtools.pw/images/files/embed/logo.png',
      },
      fields: [{
        name: "The invite link for SteamTools : ",
        value: "https://discordapp.com/oauth2/authorize?client_id=379109549514293248&scope=bot&permissions=117760"
      }]
    })
  }
};