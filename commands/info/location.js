const {
  Command
} = require('discord.js-commando');
const jsonfile = require('jsonfile')
const fs = require('fs')

module.exports = class priceCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'location',
      group: 'info',
      memberName: 'location',
      description: 'Set the desired location for the server.',
      examples: ['location us'],
      guildOnly: true,
      throttling: {
        usages: 2,
        duration: 3
      },
      args: [{
        key: 'loc',
        prompt: 'What is the country code(eg. us, gb, ru)',
        type: 'string'
      }]
    });
  }

  run(msg, {
    sym,
    loc
  }) {
    let codes = fs.readFileSync('./util/cc.json')
    codes = JSON.parse(codes)
    loc = loc.toUpperCase()
    if (!codes[loc]) return msg.embed({
      color: 0x170420,
      author: {
        name: "SteamTools",
        icon_url: 'https://steamtools.pw/images/files/embed/logo.png',
      },
      description: `Thats not a valid country code!`
    })
    if (loc === 'US') {
      if (!msg.client.provider.get(msg.guild, "location")) {
        return msg.embed({
          color: 0x170420,
          author: {
            name: "SteamTools",
            icon_url: 'https://steamtools.pw/images/files/embed/logo.png',
          },
          description: `Server location set to **${codes[loc]} ** :flag_${loc.toLowerCase()}:`
        })
      }
      msg.client.provider.remove(msg.guild, "location")
      return msg.embed({
        color: 0x170420,
        author: {
          name: "SteamTools",
          icon_url: 'https://steamtools.pw/images/files/embed/logo.png',
        },
        description: `Server location set to **${codes[loc]} ** :flag_${loc.toLowerCase()}:`
      })

    } else {
      msg.client.provider.set(msg.guild, "location", loc)
      return msg.embed({
        color: 0x170420,
        author: {
          name: "SteamTools",
          icon_url: 'https://steamtools.pw/images/files/embed/logo.png',
        },
        description: `Server location set to **${codes[loc]} ** :flag_${loc.toLowerCase()}:`
      })
    }



  }
};