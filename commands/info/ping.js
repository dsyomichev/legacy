const oneLine = require('common-tags').oneLine;
const {
  Command
} = require('discord.js-commando');

module.exports = class PingCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'ping',
      group: 'info',
      memberName: 'ping',
      description: 'Checks the bot\'s ping to the Discord server.',
      throttling: {
        usages: 5,
        duration: 10
      }
    });
  }

  async run(msg) {
    const pingMsg = await msg.embed({
      color: 0x170420,
      author: {
        name: 'SteamTools',
        icon_url: 'https://steamtools.pw/images/files/embed/logo.png',
      },
      description: `Pinging...`,
    })
    return pingMsg.edit({
      embed: {
        color: 0x170420,
        author: {
          name: 'SteamTools',
          icon_url: 'https://steamtools.pw/images/files/embed/logo.png',
        },
        description: `:left_right_arrow: | **Round Trip** : ${pingMsg.createdTimestamp - msg.createdTimestamp}ms\n:heart: | **Heartbeat** : ${this.client.ping ? `${Math.round(this.client.ping)}ms.` : ''}`,
      }
    });
  }
};