const moment = require('moment');
require('moment-duration-format');
const {
  version
} = require('../../util/config.json');
const {
  Command
} = require('discord.js-commando');

module.exports = class statsCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'stats',
      group: 'info',
      memberName: 'stats',
      description: 'Displays Server Stats',
      examples: ['stats'],
      throttling: {
        usages: 2,
        duration: 3
      }
    });
  }
  run(msg) {
    return msg.embed({
      color: 0x170420,
      author: {
        name: "SteamTools",
        icon_url: 'https://steamtools.pw/images/files/embed/logo.png',
      },
      description: "Just Some Stats...",
      fields: [{
          name: '~Uptime',
          value: moment.duration(this.client.uptime)
            .format('d[ Days], h[ Hours], m[ Minutes]'),
          inline: true

        },
        {
          name: ' ~Memory usage',
          value: `${Math.round(process.memoryUsage().heapUsed / 1024 / 1024)}MB`,
          inline: true

        }, {
          name: '~General Stats',
          value: `• Guilds: ${this.client.guilds.size}\n• Channels: ${this.client.channels.size}\n• Users: ${this.client.guilds.map(guild => guild.memberCount).reduce((a, b) => a + b)}
                    `,
          inline: true
        },

        {
          name: '~Developed By',
          value: `bObO#1055`,
          inline: true

        }
      ],
      thumbnail: {
        url: msg.guild.me.user.avatarURL
      }
    });
  }
}