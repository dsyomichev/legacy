const {
  Command
} = require('discord.js-commando');
const st = require('../../modules/steamtools')
module.exports = class steamCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'achieve',
      group: 'store',
      memberName: 'achieve',
      description: 'Searches for an achievment by game.',
      examples: ['achieve <game> <achievement name>'],
      guildOnly: true,
      throttling: {
        usages: 2,
        duration: 3,
      },
      args: [{
          key: "input",
          prompt: "What is the appID or name of the game?",
          type: "string",
        },
        {
          key: "name",
          prompt: "What is the name of the achievment?",
          type: "string"
        }
      ]
    });
  }
  async run(msg, {
    input,
    name
  }) {
    let search = await msg.embed(st.search())
    let store, players, appid
    if (input.match(/^[0-9]+$/) !== null) {
      let storeResult = st.getStore(input)
      store = await storeResult
      if (store === -1) {
        return search.edit({
          embed: st.gameError()
        })
      }
      appid = input
    } else {
      let fuzzy = st.searchGame(input)
      fuzzy = await fuzzy
      if (fuzzy === -1) {
        return search.edit({
          embed: st.gameError()
        })
      }
      appid = fuzzy.appid
    }
    let array = await st.exAchiev(appid)
    let achieve = await st.achievSearch(array, name)
    if (achieve === -1) {
      return search.edit({
        embed: st.achieveError()
      })
    }

    return search.edit({
      embed: {
        author: {
          name: achieve.name,
          icon_url: 'https://steamtools.pw/images/files/embed/logo.png'
        },
        title: `http://steamcommunity.com/stats/${appid}/achievements`,
        color: 0x161182,
        fields: [{
            name: "Percent of Owners",
            value: achieve.percent,
            inline: false

          },
          {
            name: "Description",
            value: achieve.description,
            inline: false

          },
        ],
        thumbnail: {
          url: achieve.url
        }
      }
    })
  }
}