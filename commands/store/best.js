const {
  Command
} = require('discord.js-commando');
const st = require('../../modules/steamtools')
module.exports = class steamCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'best',
      group: 'store',
      memberName: 'best',
      description: 'Top 5 Best Sellers on Steam.',
      examples: ['best'],
      guildOnly: true,
      throttling: {
        usages: 2,
        duration: 3,
      }
    });
  }
  async run(msg) {
    let cc = msg.client.provider.get(msg.guild, "location")
    let search = await msg.embed(st.search())
    let best = await st.exBestSellers(cc)
    let field = "Top 5 Best Sellers.\n\n"
    for (let i = 0; i < 5; i++) {
      let tmp = `**${best[i].name}** : ${best[i].price}\n\n`
      field = field + tmp
    }
    return search.edit({
      embed: {
        author: {
          name: 'Best Sellers on Steam',
          icon_url: 'https://steamtools.pw/images/files/embed/logo.png'

        },
        description: field,
        color: 0x161182,
        thumbnail: {
          url: `https://steamtools.pw/images/files/embed/up.png`,
        }
      }

    })

  }
}