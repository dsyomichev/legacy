const { Command } = require("discord.js-commando");
const st = require("../../modules/steamtools");
module.exports = class steamCommand extends Command {
  constructor(client) {
    super(client, {
      name: "game",
      group: "store",
      memberName: "game",
      description: "Searches the Steam Store for a game",
      examples: ["game <game name>"],
      guildOnly: true,
      throttling: {
        usages: 2,
        duration: 3
      },
      args: [
        {
          key: "input",
          prompt: "What is the appID or name of the game?",
          type: "string"
        }
      ]
    });
  }
  async run(msg, { input }) {
    let cc = msg.client.provider.get(msg.guild, "location");
    let search = await msg.embed(st.search());
    let store, players, appid;
    if (input.match(/^[0-9]+$/) !== null && input.length === 6) {
      let storeResult = st.getStore(input);
      let playersResult = st.getPlayers(input);
      store = await storeResult;
      if (store === -1) {
        return search.edit({
          embed: st.gameError()
        });
      }
      players = await playersResult;
      if (players === -1) {
        players = "N/A";
      }
      appid = input;
    } else {
      let fuzzy = st.searchGame(input, cc);
      fuzzy = await fuzzy;
      if (fuzzy === -1) {
        return search.edit({
          embed: st.gameError()
        });
      }
      store = fuzzy.json;
      players = fuzzy.players;
      appid = fuzzy.appid;
    }
    let final, discount;
    if (store[appid].data.is_free === true) {
      final = "Free";
      discount = "None";
    } else {
      if (store[appid].data.price_overview) {
        let cc = store[appid].data.price_overview.currency;
        cc = await st.codeToSymbol(cc);
        let nonPrice = JSON.stringify(store[appid].data.price_overview.final);
        let dollar = nonPrice.slice(0, -2);
        let cent = nonPrice.slice(-2);
        discount =
          JSON.stringify(store[appid].data.price_overview.discount_percent) +
          "%";
        final = cc + dollar + "." + cent;
      } else {
        final = "N/A";
        discount = "0%";
      }
    }
    let title = store[appid].data.name;
    let type = store[appid].data.type.toUpperCase();
    let dev = store[appid].data.developers[0];
    let pub = store[appid].data.publishers[0];
    let meta;
    if (!store[appid].data.metacritic) {
      meta = "N/A";
    } else {
      meta = store[appid].data.metacritic.score;
    }
    let pc, mac, lin;
    if (store[appid].data.platforms.windows === true) {
      pc = "<:stcheck:392069835061985283>";
    } else {
      pc = "<:stex:392069849918078976>";
    }
    if (store[appid].data.platforms.mac === true) {
      mac = "<:stcheck:392069835061985283>";
    } else {
      mac = "<:stex:392069849918078976>";
    }
    if (store[appid].data.platforms.linux === true) {
      lin = "<:stcheck:392069835061985283>";
    } else {
      lin = "<:stex:392069849918078976>";
    }
    return search.edit({
      embed: {
        author: {
          name: title,
          icon_url: "https://steamtools.pw/images/files/embed/logo.png"
        },
        title: `http://store.steampowered.com/app/${appid}`,
        color: 0x161182,
        fields: [
          {
            name: "Current Price",
            value: final,
            inline: true
          },
          {
            name: "Discount",
            value: discount,
            inline: true
          },
          {
            name: "Current Players",
            value: players,
            inline: true
          },
          {
            name: "Metacritic",
            value: meta,
            inline: true
          },
          {
            name: "Developer",
            value: dev,
            inline: true
          },
          {
            name: "Publisher",
            value: pub,
            inline: true
          },
          {
            name: "Support",
            value: `Windows : ${pc} | Mac : ${mac} | Linux : ${lin}`,
            inline: false
          }
        ],
        thumbnail: {
          url: `http://cdn.akamai.steamstatic.com/steam/apps/${appid}/header.jpg`
        }
      }
    });
  }
};
