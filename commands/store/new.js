const {
  Command
} = require('discord.js-commando');
const st = require('../../modules/steamtools')
module.exports = class steamCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'new',
      group: 'store',
      memberName: 'new',
      description: 'Returns the top 5 New Releases on Steam.',
      examples: ['new'],
      guildOnly: true,
      throttling: {
        usages: 2,
        duration: 3,
      }
    });
  }
  async run(msg) {
    let cc = msg.client.provider.get(msg.guild, "location")
    let search = await msg.embed(st.search())
    let newRel = await st.exNew(cc)
    let field = "Top 5 New Releases.\n\n"
    for (let i = 0; i < 5; i++) {
      let tmp = `**${newRel[i].name}** : ${newRel[i].price}\n\n`
      field = field + tmp
    }


    return search.edit({
      embed: {
        author: {
          name: 'Top 5 New Releases on Steam',
          icon_url: 'https://steamtools.pw/images/files/embed/logo.png'
        },
        description: field,
        color: 0x161182,
        thumbnail: {
          url: `https://steamtools.pw/images/files/embed/new.png`,
        }

      }
    })

  }
}