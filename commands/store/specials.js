const {
  Command
} = require('discord.js-commando');
const st = require('../../modules/steamtools')
module.exports = class steamCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'specials',
      group: 'store',
      memberName: 'specials',
      description: 'Returns the top 5 specials on Steam.',
      examples: ['specials'],
      guildOnly: true,
      throttling: {
        usages: 2,
        duration: 3,
      }
    });
  }
  async run(msg) {
    let cc = msg.client.provider.get(msg.guild, "location")
    let search = await msg.embed(st.search())
    let special = await st.exSpecials(cc)
    let field = "Top 5 Specials.\n\n"
    for (let i = 0; i < 5; i++) {
      let tmp = `**${special[i].name}** : ${special[i].price} | ~~${special[i].original}~~\n\n`
      field = field + tmp
    }

    return search.edit({
      embed: {
        author: {
          name: 'Top 5 Specials on Steam',
          icon_url: 'https://steamtools.pw/images/files/embed/logo.png'
        },
        description: field,
        color: 0x161182,
        thumbnail: {
          url: `https://steamtools.pw/images/files/embed/spe.png`,
        }

      }
    })

  }
}