const {
  Command
} = require('discord.js-commando');
const st = require('../../modules/steamtools')
module.exports = class steamCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'top',
      group: 'store',
      memberName: 'top',
      description: 'Returns the Top 5 played games on steam at the time.',
      examples: ['top'],
      guildOnly: true,
      throttling: {
        usages: 2,
        duration: 3,
      }
    });
  }
  async run(msg) {
    let search = await msg.embed(st.search())
    let top = await st.exTop()

    let field = []
    for (let i = 0; i < 5; i++) {
      field.push({
        name: top[i].name,
        value: `Current Players : ${top[i].current} | Today's Peak : ${top[i].peak}`,
        inline: false
      })
    }
    return search.edit({
      embed: {
        author: {
          name: 'Top 10 played games on Steam.',
          icon_url: 'https://steamtools.pw/images/files/embed/logo.png'
        },
        description: 'Top 5 played games by players.',
        fields: field,
        color: 0x161182,
        thumbnail: {
          url: 'https://steamtools.pw/images/files/embed/uptop.png',
        }
      }

    })

  }
}