const {
  Command
} = require('discord.js-commando');
const st = require('../../modules/steamtools')
module.exports = class steamCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'upcoming',
      group: 'store',
      memberName: 'upcoming',
      description: 'Returns the top 5 upcoming games on Steam.',
      examples: ['upcoming'],
      guildOnly: true,
      throttling: {
        usages: 2,
        duration: 3,
      }
    });
  }
  async run(msg) {
    let cc = msg.client.provider.get(msg.guild, "location")
    let search = await msg.embed(st.search())
    let up = await st.exUpcoming(cc)
    let field = "Top 5 Upcoming Games.\n\n"
    for (let i = 0; i < 5; i++) {
      let tmp = `**${up[i].name}** : ${up[i].date}\n\n`
      field = field + tmp
    }


    return search.edit({
      embed: {
        author: {
          name: 'Top 5 Upcoming Games on Steam',
          icon_url: 'https://steamtools.pw/images/files/embed/logo.png'
        },
        description: field,
        color: 0x161182,
        thumbnail: {
          url: `https://steamtools.pw/images/files/embed/upcoming.png`,
        }
      }

    })

  }
}