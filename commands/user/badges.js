const {
  Command
} = require('discord.js-commando');
const st = require('../../modules/steamtools')
const u = require("underscore")
const moment = require("moment")
module.exports = class steamCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'badges',
      group: 'user',
      memberName: 'badges',
      description: 'Displays a Badge from a game.',
      details: 'Unable to display badges not connected to a game!',
      examples: ['badges <user> <game name>'],
      guildOnly: true,
      throttling: {
        usages: 1,
        duration: 300,
      },
      args: [{
          key: "subject",
          prompt: "What is the SteamID64, or CustomID of the user?",
          type: "string",
        },
        {
          key: "app",
          prompt: "What is the name or appID of the Game?",
          type: "string"

        }
      ]
    });
  }
  async run(msg, {
    subject,
    app
  }) {
    let search = await msg.embed(st.search())
    let steamid
    if (subject.match(/^[0-9]+$/) !== null && subject.length === 17) {
      steamid = subject
    } else {
      steamid = await st.getID64(subject)
    }
    if (steamid === -1) {
      return search.edit({
        embed: st.userError()
      })
    }
    let summary = st.getSummary(steamid)
    let badges = st.getBadges(steamid)
    summary = await summary;
    if (summary === -1) {
      return search.edit({
        embed: st.userError()
      })
    }
    if (summary.communityvisibilitystate === 1) {
      return search.edit({
        embed: st.privateError()
      })
    }
    badges = await badges
    if (badges === -1) {
      return search.edit({
        embed: st.badgeError()
      })
    }

    let store, appid
    if (app.match(/^[0-9]+$/) !== null) {
      let storeResult = st.getStore(app)
      store = await storeResult
      if (store === -1) {
        return search.edit({
          embed: st.gameError()
        })
      }
      appid = parseInt(app)
    } else {
      let fuzzy = st.searchGame(app)
      fuzzy = await fuzzy
      if (fuzzy === -1) {
        return search.edit({
          embed: st.gameError()
        })
      }
      store = fuzzy.json
      appid = fuzzy.appid
    }
    let gameBadge = u.findWhere(badges, {
      appid: appid
    })
    if (!gameBadge) {
      return search.edit({
        embed: st.badgeError()
      })
    }
    gameBadge.name = await st.exBadgesAppName(summary.profileurl, gameBadge.appid)
    if (gameBadge.name === -1) {
      return search.edit({
        embed: st.badgeError()
      })
    }
    return search.edit({
      embed: {
        author: {
          name: summary.personaname,
          icon_url: summary.avatarfull
        },
        description: '**' + gameBadge.name + '**',
        color: 0x161182,
        fields: [{
            name: 'Level : ',
            value: 'Level ' + gameBadge.level,
            inline: true
          },
          {
            name: 'Date Unlocked : ',
            value: moment.unix(gameBadge.completion_time).format("MM/DD/YYYY"),
            inline: true
          },
          {
            name: 'XP for completion : ',
            value: gameBadge.xp + ' XP',
            inline: true
          }
        ],
        thumbnail: {
          url: 'https://steamtools.pw/images/files/embed/badge.png'
        },
        footer: {
          text: "Game : " + store[appid].data.name
        }
      }
    })
  }
}