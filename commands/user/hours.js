const {
  Command
} = require('discord.js-commando');
const st = require('../../modules/steamtools')
const u = require("underscore")
module.exports = class steamCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'hours',
      group: 'user',
      memberName: 'hours',
      description: 'Searches for the number of hours a user has played a game.',
      examples: ['hours <user> <game name | top*>'],
      guildOnly: true,
      throttling: {
        usages: 2,
        duration: 3,
      },
      args: [{
        key: "user",
        prompt: "What is the SteamID64, or CustomID of the user?",
        type: "string",
      }, {
        key: "game",
        prompt: "What is the game or appID to check hours for.",
        type: "string",
        default: ""
      }]
    });
  }
  async run(msg, {
    user,
    game
  }) {
    //Search Message
    let search = await msg.embed(st.search())
    let steamid = st.getID64(user)

    //Convert CustomID to SteamID64
    if (user.match(/^[0-9]+$/) && user.length === 17) {
      steamid = user
    } else {
      steamid = await steamid
    }
    if (steamid === -1) {
      return search.edit({
        embed: st.userError()
      })
    }
    let summary = st.getSummary(steamid)
    summary = await summary
    if (summary === -1) {
      return search.edit({
        embed: st.userError()
      })
    }
    if (game) {
      //Resolve appid from name
      let store, appid
      if (game.match(/^[0-9]+$/) !== null && game.length === 6) {
        let storeResult = st.getStore(game)
        store = await storeResult
        if (store === -1) {
          return search.edit({
            embed: st.gameError()
          })
        }
        appid = game
      } else {
        let fuzzy = st.searchGame(game, cc)
        fuzzy = await fuzzy
        if (fuzzy === -1) {
          return search.edit({
            embed: st.gameError()
          })
        }
        store = fuzzy.json
        players = fuzzy.players
        appid = fuzzy.appid
      }

      let games = await st.getOwned(steamid)
      if (games === -1) {
        return search.edit({
          embed: st.gameError()
        })
      }
      let gameHours = u.findWhere(games.games, {
        appid: parseInt(appid)
      })
      if (!gameHours) {
        return search.edit({
          embed: st.gameError()
        })
      }

      gameHours.name = store[appid].data.name

      let twoweeks
      if (gameHours.playtime_2weeks) {
        twoweeks = Math.round(gameHours.playtime_2weeks / 60)
      } else twoweeks = 0


      return search.edit({
        embed: {
          author: {
            name: summary.personaname,
            icon_url: summary.avatarfull
          },
          color: 0x161182,
          description: `**${gameHours.name}**`,
          fields: [{
            name: "Total Hours : ",
            value: Math.round(gameHours.playtime_forever / 60) + ' Hours',
            inline: true
          },
          {
            name: "Last 2 Weeks : ",
            value: twoweeks + ' Hours',
            inline: true
          },
          ],
          thumbnail: {
            url: `http://cdn.akamai.steamstatic.com/steam/apps/${appid}/header.jpg`
          }
        }

      })

    } else if (!game) {
      let games = await st.getOwned(steamid)
      if (games === -1) {
        return search.edit({
          embed: st.gameError()
        })
      }
      games = u.sortBy(games.games, 'playtime_forever').reverse()
      let count = games.length
      if (count > 5) count = 5
      let top10 = []
      for (let i = 0; i < count; i++) {
        top10.push(games[i])
        top10[i].store = st.getStore(top10[i].appid)

        top10[i].playtime_forever = Math.round(top10[i].playtime_forever / 60)
      }
      let field = 'Top 5 played games.\n\n'
      for (let i = 0; i < count; i++) {
        top10[i].store = await top10[i].store
        if (top10[i].store === -1) top10[i].store = "N/A"
        top10[i].store = top10[i].store[top10[i].appid].data.name

        let tmp = `**${top10[i].store}** : ${top10[i].playtime_forever} Hours\n\n`
        field = field + tmp
      }

      return search.edit({
        embed: {
          author: {
            name: summary.personaname,
            icon_url: summary.avatarfull
          },
          description: field,
          color: 0x161182,
          thumbnail: {
            url: 'https://steamtools.pw/images/files/embed/clock.png'
          }

        }

      })



    }
  }
}