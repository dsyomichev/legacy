const {
  Command
} = require('discord.js-commando');
const st = require('../../modules/steamtools')
const moment = require('moment')
module.exports = class steamCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'user',
      group: 'user',
      memberName: 'user',
      description: 'Searches Steam Community for a user based on CustomID or SteamID64',
      examples: ['user <steamID64 | CustomID>'],
      guildOnly: true,
      throttling: {
        usages: 1,
        duration: 300,
      },
      args: [{
        key: 'input',
        prompt: 'What is the SteamID64, or CustomID of the user?',
        type: 'string',
      }]
    });
  }
  async run(msg, {
    input
  }) {
    //Search Message
    let search = await msg.embed(st.search())
    //Convert CustomID to SteamID64
    let steamid
    if (input.match(/^[0-9]+$/) && input.length === 17) {
      steamid = input
    } else {
      steamid = await st.getID64(input)
    }
    if (steamid === -1) {
      return search.edit({
        embed: st.userError()
      })
    }
    //Default API Requests
    let summary = st.getSummary(steamid)
    let bans = st.getBans(steamid)
    let owned = st.getOwned(steamid)
    let recent = st.getRecentlyPlayed(steamid)
    let friends = st.getFriends(steamid)
    //Recieve Player Summary
    summary = await summary
    if (summary === -1) {
      return search.edit({
        embed: st.userError()
      })
    }
    let iso = summary.loccountrycode
    let country
    if (iso) {
      country = st.isoCountry(iso)
    }
    let name = summary.personaname
    let url = summary.profileurl
    let avatar = summary.avatarfull
    let ids = `**SteamID64** : ${summary.steamid}\n**SteamID32** : ${summary.steamid - 76561197960265728}`
    let status
    if (summary.personastate === 0) {
      status = 'Offline'
    } else if (summary.personastate === 1) {
      status = 'Online'
    } else if (summary.personastate === 2) {
      status = 'Busy'
    } else if (summary.personastate === 3) {
      status = 'Away'
    } else if (summary.personastate === 4) {
      status = 'Snooze'
    } else if (summary.personastate === 5) {
      status = 'Looking to Trade'
    } else if (summary.personastate === 6) {
      status = 'Looking to Play'
    }
    //Set Privacy State
    let privacy
    if (summary.communityvisibilitystate === 1) privacy = -1
    if (summary.communityvisibilitystate === 3) privacy = 1
    //Privacy: Public
    if (privacy === 1) {
      //Recieve remaining calls
      bans = await bans
      if (bans === -1) {
        bans = 'N/A'
      }
      owned = await owned
      if (owned === -1) owned = 'N/A'
      recent = await recent
      if (recent === -1) recent = 'None'
      friends = await friends
      if (friends === -1) friends = 'N/A'
      if (summary.loccountrycode) {
        country = await country
        if (country === -1) country = 'N/A'
      } else {
        country = 'Not Set'
      }
      let cm, vb, tb
      if (bans.CommunityBanned === true) {
        cm = '<:stcheck:392069835061985283>'
      } else {
        cm = '<:stex:392069849918078976>'
      }
      if (bans.VACBanned === true) {
        vb = '<:stcheck:392069835061985283>'
      } else {
        vb = '<:stex:392069849918078976>'
      }
      if (!bans.EconomyBan === 'none') {
        tb = '<:stcheck:392069835061985283>'
      } else {
        tb = '<:stex:392069849918078976>'
      }
      bans = `**Community Ban** : **${cm}** | **Trade Ban** : **${tb}** | **VAC Ban** : **${vb}**   `
      owned = owned.game_count
      if (recent !== "None") {
        let recentList = ''
        for (let i = 0; i < recent.length; i++) {
          if (recent[i].name === undefined) {
            recent[i] = recent[(i + 1)]
            recent.pop()
          }
          recentList = recentList.concat(recent[i].name + '\n')
        }
        recent = recentList
      }
      let realName
      if (!summary.realname) {
        realName = 'Not Set'
      } else {
        realName = summary.realname
      }
      let game
      if (!summary.gameextrainfo) {
        game = 'Not Playing'
      } else {
        game = summary.gameextrainfo
      }
      let userInfo = `**Privacy State** : Public\n**Status** : ${status}\n**Game** : ${game}\n**Real Name** : ${realName}\n**Location** : ${country}`
      friends = friends.length
      //Return Message
      return search.edit({
        embed: {
          author: {
            name: name,
            icon_url: avatar
          },
          title: url,
          color: 0x161182,
          thumbnail: {
            url: avatar,
          },
          footer: {
            text: `Last logged off ${moment.unix(summary.lastlogoff).fromNow()}`
          },
          fields: [{
              name: 'User Information',
              value: userInfo + `\n**Friends** : ${friends}`,
              inline: true
            },
            {
              name: 'Steam IDs',
              value: ids,
              inline: true
            },
            {
              name: 'Total Games',
              value: owned,
              inline: true
            },
            {
              name: 'Recent Games',
              value: recent,
              inline: true
            },
            {
              name: 'Bans',
              value: bans,
              inline: true
            },

          ]
        }
      })
      //Privacy State: Private
    } else if (privacy === -1) {
      //Recive remaining calls
      let userInfo = `**Privacy State** : Private\n**Status** : ${status}`
      //Return Message
      return search.edit({
        embed: {
          author: {
            name: name,
            icon_url: avatar
          },
          title: url,
          color: 0x161182,
          thumbnail: {
            url: avatar,
          },
          footer: {
            text: `Last Log Off : ${moment.unix(summary.lastlogoff).fromNow()}`
          },
          fields: [{
              name: 'User Information',
              value: userInfo,
              inline: true
            },
            {
              name: 'Steam IDs',
              value: ids,
              inline: true
            }
          ]
        }
      })
    }
  }
}