const {
  CommandoClient
} = require('discord.js-commando');
const snekfetch = require('snekfetch')
const config = require("../util/config.json");
exports.run = (client, guild) => {
  if (config.version != -1) {
    client.guilds.get('334361317576146944').channels.get('380401610909548544').sendEmbed({
      author: {
        name: client.user.username,
        icon_url: client.user.avatarURL
      },
      color: 0xf44242,
      author: {
        name: "SteamTools",
        icon_url: 'https://steamtools.pw/images/files/embed/logo.png',
      },
      title: `Left Server : ${guild.name}`,
      description: `**Guild ID** : ${guild.id}\n**Owner** : <@${guild.ownerID}>\n**Region** : ${guild.region.toUpperCase()}\n**Member Count** : ${guild.memberCount}`,
      footer: {
        text: `Server Count : ${client.guilds.size}`
      },
      timestamp: new Date(),
      thumbnail: {
        url: guild.iconURL
      }
    })
    snekfetch.post(`https://discordbots.org/api/bots/stats`)
      .set('Authorization', config.dblToken)
      .send({
        server_count: client.guilds.size
      })
      .then(() => console.log('Updated DBL Server Count'))
      .catch(err => console.error(`Error : ${err.body}`));
  }
}