const config = require("../util/config.json");
const snekfetch = require('snekfetch')
exports.run = (client) => {
  console.log('Using Discord Token :', config.token);
  console.log('Using SteamAPI Key  :', config.key);
  console.log('SteamTools Version  :', config.version == -1 ? 'TESTING' : config.version);
  console.log('SteamTools Running')
  console.log('')

  client.user.setPresence({
    game: {
      name: '+ST help',
      url: 'https://www.twitch.tv/monstercat  ',
      type: 1
    }
  })

  if (config.version = !-1) {
    snekfetch.post(`https://discordbots.org/api/bots/stats`)
      .set('Authorization', config.dblToken)
      .send({
        server_count: client.guilds.size
      })
      .then(() => console.log('Updated DBL Server Count.'))
      .catch(function(err) {
        if (config.version === 'Testing') console.log("Not Posting Server Count | Testing Bot")
        else {
          console.error(`Error : ${err.body}`)
        }
      })
  }
};