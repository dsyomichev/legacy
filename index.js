//||\\DISCORD.JS REQUIRED MODULES//||\\
const sqlite = require("sqlite");
const { CommandoClient, SQLiteProvider } = require("discord.js-commando");
const config = require("./util/config.json");
const path = require("path");
const fs = require("fs");
const sys = require("./modules/system");
const CronJob = require("cron").CronJob;
const jsonfile = require("jsonfile");
const request = "request";
const client = new CommandoClient({
  owner: "258328359040188418",
  disableEveryone: true,
  unknownCommandResponse: false,
  commandPrefix: `+st`
});

//\\Discord//\\
sqlite.open(path.join(__dirname, "settings", "settings.sqlite3")).then(db => {
  client.setProvider(new SQLiteProvider(db));
});
client.dispatcher.addInhibitor(msg => {
  if (
    msg.channel.permissionsFor(msg.guild.me).hasPermission("SEND_MESSAGES") ===
    false
  )
    return "Missing Perms";
  return false;
});
client.registry
  .registerDefaultTypes()
  .registerGroups([
    ["info", "SteamTools Information"],
    ["config", "SteamTools Configuration"],
    ["user", "Steam User"],
    ["store", "Steam Store"],
    ["community", "Steam Community"]
  ])
  .registerCommandsIn(path.join(__dirname, "commands"));
fs.readdir("./events/", (err, files) => {
  if (err) return console.error(err);
  files.forEach(file => {
    let eventFunction = require(`./events/${file}`);
    let eventName = file.split(".")[0];
    client.on(eventName, (...args) => eventFunction.run(client, ...args));
  });
});

client.login(config.token);

//\\Cron Tabs//\\
new CronJob(
  "0 0 * * 0-6",
  async function() {
    request(
      {
        url: "http://api.steampowered.com/ISteamApps/GetAppList/v0002/"
      },
      function(error, response, body) {
        let obj = JSON.parse(body);
        let d = new Date();
        let file = "./util/gameList.json";
        jsonfile.writeFileSync(file, obj);
      }
    );
  },
  null,
  true,
  "America/New_York"
);
new CronJob(
  "*/1 * * * *",
  async function() {
    let obj = {};
    let clients = {};
    let d = new Date();
    let file = "./util/stats.json";
    clients.guilds = client.guilds.size;
    clients.channels = client.channels.size;
    clients.user = client.guilds
      .map(guild => guild.memberCount)
      .reduce((a, b) => a + b);
    clients.uptime = client.uptime;
    clients.memory = process.memoryUsage().heapUsed;
    obj.process = clients;
    obj.os = await sys.os();
    obj.date = `${d.getHours()}:${d.getMinutes()} ${d.getDate()}/${d.getMonth() +
      1}/${d.getFullYear()}`;
    jsonfile.writeFileSync(file, obj);
  },
  null,
  true,
  "America/New_York"
);
