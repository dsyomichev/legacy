const rp = require('request-promise');
const {
  key
} = require('../util/config.json');
const u = require('underscore')
const f = require("fuse-js-latest")
const j = require('jsonfile')
const symbol = require('currency-symbol-map')
module.exports = {
  //Steam API Calls
  getID64: async function(customID) {
    let result;
    let apiRequest = rp(`http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key=${key}&vanityurl=${customID}`);
    try {
      result = await apiRequest;
    } catch (err) {
      return -1;
    }
    if (result === undefined) return -1
    result = JSON.parse(result);
    if (result.response.success !== 1) {
      return -1;
    } else if (result.response.success === 1) {
      return result.response.steamid;
    }
  },
  getSummary: async function(steamID) {
    let result;
    let apiRequest = rp(`http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=${key}&steamids=${steamID}`);
    try {
      result = await apiRequest
    } catch (err) {
      return -1;
    }
    if (result === undefined) return -1
    result = JSON.parse(result)
    if (result.response.players.length === 0) return -1
    return result.response.players[0]

  },
  getBans: async function(steamID) {
    let result;
    let apiRequest = rp(`http://api.steampowered.com/ISteamUser/GetPlayerBans/v1/?key=${key}&steamids=${steamID}`);
    try {
      result = await apiRequest
    } catch (err) {
      return -1;
    }
    if (result === undefined) return -1
    result = JSON.parse(result)
    return result.players[0]
  },
  getOwned: async function(steamID) {
    let result;
    let apiRequest = rp(`http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=${key}&steamid=${steamID}`);
    try {
      result = await apiRequest
    } catch (err) {
      return -1;
    }
    if (result === undefined) return -1
    result = JSON.parse(result)
    if (u.isEmpty(result.response)) {
      return -1
    } else {
      return result.response
    }
  },
  getRecentlyPlayed: async function(steamID) {
    let result;
    let apiRequest = rp(`http://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v0001/?key=${key}&steamid=${steamID}&count=3`);
    try {
      result = await apiRequest
    } catch (err) {
      return -1;
    }
    if (result === undefined) return -1
    result = JSON.parse(result)
    if (result.response.total_count === 0) {
      return -1
    } else {
      return result.response.games
    }
  },
  getFriends: async function(steamID) {
    let result;
    let apiRequest = rp(`http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key=${key}&steamid=${steamID}&relationship=friend`);
    try {
      result = await apiRequest
    } catch (err) {
      return -1
    }
    if (result === undefined) return -1
    result = JSON.parse(result)
    return result.friendslist.friends
  },
  getStore: async function(appid) {
    let result
    let apiRequest = rp(`http://store.steampowered.com/api/appdetails?appids=${appid}&cc=us`)
    try {
      result = await apiRequest
    } catch (err) {
      return -1
    }
    if (result === undefined) return -1
    result = JSON.parse(result)
    if (result[appid].success === false) {
      return -1
    } else if (result[appid].success === true) {
      return result
    }
  },
  getPlayers: async function(appid) {
    let result
    let apiRequest = rp(`https://api.steampowered.com/ISteamUserStats/GetNumberOfCurrentPlayers/v1/?appid=${appid}`)
    try {
      result = await apiRequest
    } catch (err) {
      return -1
    }
    if (result === undefined) return -1
    result = JSON.parse(result)
    return result.response.player_count
  },
  getBadges: async function(steamid) {
    let result
    let apiRequest = rp(`https://api.steampowered.com/IPlayerService/GetBadges/v1?key=${key}&steamid=${steamid}`)
    try {
      result = await apiRequest
    } catch (err) {
      return -1
    }
    if (result === undefined) return -1
    result = JSON.parse(result)
    if (result.response.player_level === 0) {
      return -1
    }
    return result.response.badges

  },
  //Steam Page Extracts
  exBadgesSteamName: async function(url, badgeid) {
    let result
    let siteRequest = rp(`${url}badges/${badgeid}`)
    try {
      result = await siteRequest
    } catch (err) {
      return -1
    }
    if (result === undefined) return -1
    let startIndex = result.search('<div class="badge_title">')
    let endIndex = result.search('<div class="badge_title_rule">')
    result = result.substring(startIndex, endIndex)
    result = result.replace('<div class="badge_title">', '').replace(/<\/div>/g, '').replace(/\n/g, '').replace(/\r/g, '').replace(/\t/g, '')
    return result

  },
  exBadgesAppName: async function(url, appid) {
    let result
    let siteRequest = rp(`${url}gamecards/${appid}`)
    try {
      result = await siteRequest
    } catch (err) {
      return -1
    }
    if (result === undefined) return -1
    let startIndex = result.search('<div class="badge_info_description">')
    let endIndex = result.search('<div class="badge_info_unlocked">')
    result = result.substring(startIndex, endIndex)
    startIndex = result.search('<div class="badge_info_title">')
    endIndex = result.search('</div>')
    result = result.substring(startIndex, endIndex)
    result = result.replace('<div class="badge_info_title">', '')
    return result
  },
  exAchiev: async function(appid) {
    let result
    let siteRequest = rp(`http://steamcommunity.com/stats/${appid}/achievements`)
    try {
      result = await siteRequest
    } catch (err) {
      return -1
    }
    if (result === undefined) return -1
    let achiev = []

    let html = result.replace(`<br />`, `~~~`)
    let numberStartIndex = html.search('<span class="wt">')
    let numberEndIndex = html.search(`<br />`)
    let number = html.substring(numberStartIndex, numberEndIndex)
    number = parseInt(number.replace(/<[^>]+>/g, ''));
    for (let i = 0; i < number; i++) {
      let imgStart = html.search('<div class="achieveImgHolder">')
      let imgEnd = html.search('<div class="achieveTxt">')

      let img = html.substring(imgStart, imgEnd)
      let imgRem = html.substring(imgStart, imgEnd)
      let urlStart = img.search('img src="')
      let urlEnd = img.search('" width=')
      let url = img.substring(urlStart, urlEnd)
      url = url.replace('img src="', '')
      img = img.replace(/<[^>]+>/g, '')
      img = img.replace(/\n/g, '')
      img = img.replace(/\r/g, '')
      let percent = img.replace(/\t/g, '')
      let nameDescStart = html.search('<div class="achieveTxt">')
      let nameDescEnd = html.search('</h5>')
      let nameDesc = html.substring(nameDescStart, nameDescEnd + 5)
      let nameStart = nameDesc.search("<h3>")
      let nameEnd = nameDesc.search("</h3>")
      let name = nameDesc.substring(nameStart + 4, nameEnd)
      let descStart = nameDesc.search("<h5>")
      let descEnd = nameDesc.search("</h5>")
      let desc = nameDesc.substring(descStart + 4, descEnd)
      html = html.replace(nameDesc, '')
      html = html.replace(img, '')
      let obj = {
        "name": `${name}`,
        "description": `${desc}`,
        "url": `${url}`,
        "percent": `${percent}`
      }
      html = html.replace(imgRem, '')
      achiev.push(obj)

    }
    achiev = JSON.stringify(achiev)
    achiev = JSON.parse(achiev)
    return achiev
  },
  exItem: async function(name, cc) {
    let result
    let siteRequest = rp(`http://steamcommunity.com/market/search/render/?query=${name}&start=0&count=1&cc=${cc}`)
    try {
      result = await siteRequest
    } catch (err) {
      return -1
    }
    if (result === undefined) return -1
    let json = JSON.parse(result)
    json = json.results_html
    let count = 1
    if (json === `\t<div class=\"market_listing_table_message\">There were no items matching your search. Try again with different keywords.<\/div>\r\n`) {
      return -1
    }
    let html = JSON.stringify(json)
    html = html.replace(/\\t/g, '')
    html = html.replace(/\\n/g, '')
    html = html.replace(/\\r/g, '')
    html = html.replace(/\\/g, '')
    html = html.replace(/"/g, '')
    html = html.replace('<div class=market_listing_table_header><div class=market_listing_price_listings_block><div class=market_listing_right_cell market_listing_their_price market_sortable_column data-sorttype=price>PRICE<span class=market_sort_arrow style=display:none;></span></div><div class=market_listing_right_cell market_listing_num_listings market_sortable_column data-sorttype=quantity>QUANTITY<span class=market_sort_arrow style=display:none;></span></div><div class=market_listing_right_cell market_listing_price_listings_combined market_sortable_column data-sorttype=price>PRICE<span class=market_sort_arrow style=display:none;></span></div></div><div class=market_sortable_column data-sorttype=name><span class=market_listing_header_namespacer></span>NAME<span class=market_sort_arrow style=display:none;></span></div></div>', '')
    let redact = []
    for (let i = 0; i < count; i++) {
      let imgIndexStart = html.search("src=")
      let imgIndexEnd = html.search("srcset=")
      let imgR = html.substring(imgIndexStart, imgIndexEnd + 7)
      let img = html.substring(imgIndexStart, imgIndexEnd)
      html = html.replace(imgR, '')
      img = img.replace("src=", "")
      let linkIndexStart = html.search('href=')
      let linkIndexEnd = html.search(' id=resultlink_')
      let link = html.substring(linkIndexStart, linkIndexEnd)
      html = html.replace('href=', '')
      html = html.replace(' id=resultlink_', '')
      link = link.replace("href=", '')
      let l = {
        "link": link,
        "image": img
      }
      redact.push(l)
    }
    let k = JSON.stringify(redact)
    for (let i = 0; i < count; i++) {
      html = html.replace('</a><a', `</a>
<a`)
    }
    html = html.replace(/Starting at:/g, '');
    html = html.replace(/<[^>]+>/g, '~');
    html = html.replace(/~~~~~~~~~/g, '@');
    for (let i = 0; i < count; i++) {
      html = html.replace('~~~~~~~', '!~');
      html = html.replace('~~~~~~~', '@~');
    }
    html = html.replace(/~~~~~~/g, '@&');
    html = html.replace(/~~~/g, '@~');
    html = html.replace(/~~/g, '@~')

    html = html.replace(/@/g, '",');
    html = html.replace(/~/gm, `{*`);
    html = html.replace(/,&/g, '},*');
    html = html.replace(/!/g, `{`);
    for (let i = 0; i < count; i++) {
      html = html.replace('{*', '"listings":"')
      html = html.replace('{*', '"sale":"')
      html = html.replace('{*', '"order":"')
      html = html.replace('{*', '"name":"')
      html = html.replace('{*', '"game":"')
      html = html.replace('},*', ',"link":' + `\"${redact[i].link}\"},*`)
      html = html.replace('},*', ',"image":' + `\"${redact[i].image}\"},`)
    }
    html = html.slice(0, -1)
    let j = '{"response":['
    j = j.concat(html)
    j = j.concat(']}')


    j = JSON.parse(j)
    return j.response[0]


  },
  exTop: async function() {
    let result
    let siteRequest = rp(`http://store.steampowered.com/stats`)
    try {
      result = await siteRequest
    } catch (err) {
      return -1
    }
    result = result.replace('</table>', '')
    let htmlStart = result.search('<table>')
    let htmlEnd = result.search('</table>')
    let html = result.substring(htmlStart, htmlEnd)
    let top = []
    for (let i = 0; i < 10; i++) {
      let currentStart = html.search('<span class="currentServers">')
      let currentEnd = html.search('</span>')
      let current = html.substring(currentStart, currentEnd + 7)
      html = html.replace(current, '')
      current = current.replace(/<[^>]*>/g, '')
      let peakStart = html.search('<span class="currentServers">')
      let peakEnd = html.search('</span>')
      let peak = html.substring(peakStart, peakEnd + 7)
      html = html.replace(peak, '')
      peak = peak.replace(/<[^>]*>/g, '')
      let gameStart = html.search('<a class="gameLink"')
      let gameEnd = html.search('</a>')
      let game = html.substring(gameStart, gameEnd + 4)
      html = html.replace(game, '')
      game = game.replace(/<[^>]*>/g, '')
      top.push({
        name: game,
        peak: peak,
        current: current
      })
    }
    return top
  },
  exSpecials: async function(cc) {
    let result = await rp(`http://store.steampowered.com/search/?specials=1&cc=${cc}`)
    let start = result.search('<div id = "search_result_container">')
    let end = result.search('<div class="search_pagination">')
    result = result.substring(start, end)
    let games = []
    for (let i = 0; i < 5; i++) {
      let obj = {}
      let discStart = result.search('<div class="col search_discount responsive_secondrow">')
      let discEnd = result.search('<div class="col search_price discounted responsive_secondrow">')
      let discount = result.substring(discStart, discEnd + 62)
      result = result.replace(discount, '')
      obj.discount = discount.replace(/<[^>]+>/g, '').replace(/\n/g, '').replace(/\r/g, '').replace(/\t/g, '')
      let nameStart = result.search('<span class="title">')
      let nameEnd = result.search('<span class="platform_img win"></span>')
      let name = result.substring(nameStart, nameEnd + 38)
      result = result.replace(name, '')
      name = name.replace(/<[^>]+>/g, '').replace(/\n/g, '').replace(/\r/g, '').replace(/\t/g, '')
      obj.name = name
      let priceStart = result.search('<span style="color: #888888;">')
      let priceEnd = result.search('<div style="clear: left;"></div>')
      let price = result.substring(priceStart, priceEnd + 32)
      let original = price.substring(price.search('<strike>'), price.search('</strike>'))
      result = result.replace(price, '')
      price = price.replace(original, '')
      obj.price = price.replace(/<[^>]+>/g, '').replace(/\n/g, '').replace(/\r/g, '').replace(/\t/g, '')
      obj.original = original.replace(/<[^>]+>/g, '').replace(/\n/g, '').replace(/\r/g, '').replace(/\t/g, '')
      games.push(obj)
    }
    return games

  },
  exUpcoming: async function(cc) {
    let result = await rp(`http://store.steampowered.com/search/?filter=comingsoon&cc=${cc}`)
    let start = result.search('<div id = "search_result_container">')
    let end = result.search('<div class="search_pagination">')
    result = result.substring(start, end)
    let games = []
    for (let i = 0; i < 5; i++) {
      let obj = {}
      let nameStart = result.search('<span class="title">')
      let nameEnd = result.search('<span class="platform_img win"></span>')
      let name = result.substring(nameStart, nameEnd + 38)
      result = result.replace(name, '')
      name = name.replace(/<[^>]+>/g, '').replace(/\n/g, '').replace(/\r/g, '').replace(/\t/g, '')
      obj.name = name
      let dateStart = result.search('<div class="col search_released responsive_secondrow">')
      let dateEnd = result.search('<div class="col search_reviewscore responsive_secondrow">')
      let date = result.substring(dateStart, dateEnd + 57)
      result = result.replace(date, '')
      obj.date = date.replace(/<[^>]+>/g, '').replace(/\n/g, '').replace(/\r/g, '').replace(/\t/g, '')
      games.push(obj)
    }
    return games
  },
  exBestSellers: async function(cc) {
    let result = await rp(`http://store.steampowered.com/search/?filter=globaltopsellers&cc=${cc}`)
    let start = result.search('<div id = "search_result_container">')
    let end = result.search('<div class="search_pagination">')
    result = result.substring(start, end)
    let games = []
    for (let i = 0; i < 5; i++) {
      let obj = {}
      let nameStart = result.search('<span class="title">')
      let nameEnd = result.search('<span class="platform_img win"></span>')
      let name = result.substring(nameStart, nameEnd + 38)
      result = result.replace(name, '')
      name = name.replace(/<[^>]+>/g, '').replace(/\n/g, '').replace(/\r/g, '').replace(/\t/g, '')
      obj.name = name
      result = result.replace('<div class="col search_price', '')
      let strikeStart = result.search('<strike>')
      let strikeEnd = result.search('</strike>')
      let strike = result.substring(strikeStart, strikeEnd + 9)
      result = result.replace(strike, '')
      let priceStart = result.search('<div class="col search_price')
      let priceEnd = result.search('<div style="clear: left;"></div>')
      let price = result.substring(priceStart, priceEnd + 32)
      result = result.replace(price, '')
      obj.price = price.replace(/<[^>]+>/g, '').replace(/\n/g, '').replace(/\r/g, '').replace(/\t/g, '')
      games.push(obj)
    }
    return games
  },
  exNew: async function(cc) {
    let result = await rp(`http://store.steampowered.com/explore/new/?cc=${cc}`)
    let start = result.search('<div class="home_ctn" style="overflow: visible; position: relative;">')
    let end = result.search('<div class="tab_content" id="tab_allnewreleases_content" style="display: none;">')
    result = result.substring(start, end)
    let games = []
    for (let i = 0; i < 5; i++) {
      let obj = {}
      let nameStart = result.search('<div class="tab_item_name">')
      let nameEnd = result.search('<div class="tab_item_details">')
      let name = result.substring(nameStart, nameEnd + 32)
      result = result.replace(name, '')
      name = name.replace(/<[^>]+>/g, '').replace(/\n/g, '').replace(/\r/g, '').replace(/\t/g, '')
      obj.name = name
      let priceStart = result.search('<div class="discount_final_price">')
      let priceEnd = result.search('<div class="tab_item_content">')
      let price = result.substring(priceStart, priceEnd + 30)
      result = result.replace(price, '')
      price = price.replace(/<[^>]+>/g, '').replace(/\n/g, '').replace(/\r/g, '').replace(/\t/g, '')
      obj.price = price
      games.push(obj)
    }
    return games
  },

  //Non API functions
  codeToSymbol: async function(code) {
    let sym = symbol(code)
    if (sym === undefined) return -1
    return sym
  },







  //Fuzzy Searches
  searchGame: async function(input, cc) {
    let list = j.readFileSync('./util/gameList.json')
    list = list.applist.apps;
    let options = {
      shouldSort: true,
      threshold: 0.6,
      location: 0,
      distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      keys: [
        "appid",
        "name"
      ]
    }
    let result = new f(list, options)
    result = result.search(input)
    if (result.length === 0) {
      return -1
    }
    let count = result.length
    if (result.length > 15) count = 15
    let promise = []
    for (let i = 0; i < count; i++) {
      promise[i] = rp(`http://store.steampowered.com/api/appdetails?appids=${result[i].appid}&cc=${cc}`)
    }
    let game = []
    for (let i = 0; i < count; i++) {
      let store
      try {
        store = await promise[i]
      } catch (err) {
        return -1
      }
      store = JSON.parse(store)
      if (store[result[i].appid].success === true) {
        if (store[result[i].appid].data.type === `game`) {
          if (store[result[i].appid].data.release_date.coming_soon === false) {
            game.push({
              name: result[i].name,
              appid: result[i].appid,
              json: store
            })

          }
        }
      }
    }
    if (game.length === 0) {
      return -1
    }
    for (let i = 0; i < game.length; i++) {
      promise[i] = rp(`https://api.steampowered.com/ISteamUserStats/GetNumberOfCurrentPlayers/v1/?appid=${game[i].appid}`)
    }
    for (let i = 0; i < game.length; i++) {
      let players
      try {
        players = await promise[i]
      } catch (err) {
        return -1
      }
      players = JSON.parse(players)
      if (players.response.result !== 1) {
        players = -1
      } else if (players.response.result === 1) {
        players = players.response.player_count
      }
      game[i].players = players
    }
    game = u.sortBy(game, 'players').reverse()
    return game[0]

  },
  achievSearch: async function(json, name) {
    let options = {
      shouldSort: true,
      threshold: 0.6,
      location: 0,
      distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      keys: [
        "name"
      ]
    };
    let fuse = new f(json, options)
    let searchResult = fuse.search(name);
    if (searchResult.length === 0) {
      return -1
    }
    return searchResult[0]
  },
  //Country ISO to Name
  isoCountry: async function(iso) {
    let result;
    let apiRequest = rp(`https://restcountries.eu/rest/v2/alpha/${iso}`);
    try {
      result = await apiRequest;
    } catch (err) {
      return -1;
    }
    result = JSON.parse(result);
    return result.name
  },
  //Default Embeds to Send
  search: function() {
    return {
      color: 0x00ffdd,
      author: {
        icon_url: `https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-ios7-search-strong-128.png`,
        name: `Searching...`
      }
    }
  },
  userError: function() {
    return {
      author: {
        name: `❌ No user found!`,
      },
      color: 0xff0000,
    }
  },
  gameError: function() {
    return {
      author: {
        name: `❌ No game found!`,
      },
      color: 0xff0000,
    }
  },
  privateError: function() {
    return {
      author: {
        name: `❌ User is private!`,
      },
      color: 0xff0000,
    }
  },
  badgeError: function() {
    return {
      author: {
        name: `❌ No badge found!`,
      },
      color: 0xff0000,
    }
  },
  achieveError: function() {
    return {
      author: {
        name: `❌ No Achievement found!`,
      },
      color: 0xff0000,
    }
  },
  itemError: function() {
    return {
      author: {
        name: `❌ No Item found!`,
      },
      color: 0xff0000,
    }
  },
}