const os = require('os');

module.exports = {
  //CPU Usage
  os: async function(opts) {
    let delay = ms => new Promise(resolve => setTimeout(resolve, ms));
    let cpus = os.cpus()
    let timeUsed;
    let timeUsed0 = 0;
    let timeUsed1 = 0;
    let timeIdle;
    let timeIdle0 = 0;
    let timeIdle1 = 0;
    let cpu1;
    let cpu0;
    let time;
    let obj = {}
    if (!opts) {
      opts = {
        coreIndex: -1,
        sampleMs: 1000,
      };
    } else {
      opts.coreIndex = opts.coreIndex || -1;
      opts.sampleMs = opts.sampleMs || 1000;
    }
    //check core exists
    if (opts.coreIndex < -1 ||
      opts.coreIndex >= cpus.length ||
      typeof opts.coreIndex !== 'number' ||
      Math.abs(opts.coreIndex % 1) !== 0
    ) {
      _error(opts.coreIndex, cpus.length);
      return -1
    }
    //all cpu's average
    if (opts.coreIndex === -1) {
      //take first measurement
      cpu0 = os.cpus();
      time = process.hrtime();
      await delay(opts.sampleMs);
      cpu1 = os.cpus();
      let diff = process.hrtime(time);
      let diffSeconds = diff[0] + diff[1] * 1e-9;
      //do the number crunching below and return
      for (let i = 0; i < cpu1.length; i++) {
        timeUsed1 += cpu1[i].times.user;
        timeUsed1 += cpu1[i].times.nice;
        timeUsed1 += cpu1[i].times.sys;
        timeIdle1 += cpu1[i].times.idle;
      }
      for (i = 0; i < cpu0.length; i++) {
        timeUsed0 += cpu0[i].times.user;
        timeUsed0 += cpu0[i].times.nice;
        timeUsed0 += cpu0[i].times.sys;
        timeIdle0 += cpu0[i].times.idle;
      }
      timeUsed = timeUsed1 - timeUsed0;
      timeIdle = timeIdle1 - timeIdle0;
      let percent = (timeUsed / (timeUsed + timeIdle)) * 100;
      obj.cpu = percent
    } else {
      //take first measurement
      cpu0 = os.cpus();
      time = process.hrtime();
      await delay(opts.sampleMs);
      //take second measurement
      cpu1 = os.cpus();
      let diff = process.hrtime(time);
      let diffSeconds = diff[0] + diff[1] * 1e-9;
      //do the number crunching below and return
      timeUsed1 += cpu1[opts.coreIndex].times.user;
      timeUsed1 += cpu1[opts.coreIndex].times.nice;
      timeUsed1 += cpu1[opts.coreIndex].times.sys;
      timeIdle1 += cpu1[opts.coreIndex].times.idle;
      timeUsed0 += cpu0[opts.coreIndex].times.user;
      timeUsed0 += cpu0[opts.coreIndex].times.nice;
      timeUsed0 += cpu0[opts.coreIndex].times.sys;
      timeIdle0 += cpu0[opts.coreIndex].times.idle;
      let timeUsed = timeUsed1 - timeUsed0;
      let timeIdle = timeIdle1 - timeIdle0;
      let percent = (timeUsed / (timeUsed + timeIdle)) * 100;
      obj.cpu = percent
    }
    obj.free = os.freemem()
    obj.total = os.totalmem()
    obj.used = (obj.free - obj.total)
    obj.type = os.type()
    obj.arch = os.arch()
    obj.host = os.hostname()
    obj.load = os.loadavg()
    obj.plat = os.platform()
    obj.release = os.release()
    obj.uptime = os.uptime()
    return obj
  },
  pr: async function() {
    let obj = {}

    obj.usedMem = process.memoryUsage().heapUsed
    obj.uptime = process.uptime()
    obj.version = process.version()


  }
}